package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 日K自我修复任务
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
@ApiModel(value = "日K自我修复任务")
public class ShareFixDaykTask extends Model<ShareFixDaykTask> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 第几次修复，默认0
     */
    @ApiModelProperty(value = "第几次修复，默认0")
    private Integer time;
    /**
     * 最后一次更新时间
     */
    @ApiModelProperty(value = "最后一次更新时间")
    private LocalDateTime updateTime;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取第几次修复，默认0
     */
    public Integer getTime() {
        return time;
    }

    /**
     * 设置第几次修复，默认0
     */

    public void setTime(Integer time) {
        this.time = time;
    }

    /**
     * 获取最后一次更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置最后一次更新时间
     */

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 第几次修复，默认0列的数据库字段名称
     */
    public static final String TIME = "time";

    /**
     * 最后一次更新时间列的数据库字段名称
     */
    public static final String UPDATE_TIME = "update_time";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareFixDaykTask{" +
                "id=" + id +
                ", stockId=" + stockId +
                ", time=" + time +
                ", updateTime=" + updateTime +
                "}";
    }
}
