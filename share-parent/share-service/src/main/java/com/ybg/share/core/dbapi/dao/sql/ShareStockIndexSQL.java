package com.ybg.share.core.dbapi.dao.sql;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.sql.PagerUtils;
import com.alibaba.druid.util.JdbcConstants;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;
import org.apache.ibatis.annotations.Param;

public class ShareStockIndexSQL {

    public String listStockDayK(@Param("dto") StockIndexDTO dto) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT id,ref_date,code,market,stock_name,close_price,max_price,min_price,");
        sb.append("open_price,before_close,change_amount,change_range,trade_num,trade_money");
        sb.append(" FROM share_stock_index WHERE 1=1");
        getWhereString(dto, sb);
        return PagerUtils.limit(sb.toString(), JdbcConstants.MYSQL,dto.getOffset().intValue(),dto.getSize().intValue());
    }

    public String countStockDayK(@Param("dto") StockIndexDTO dto) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(id) FROM share_stock_index WHERE 1=1");
        getWhereString(dto,sb);
        return PagerUtils.count(sb.toString(), JdbcConstants.MYSQL);
    }

    private void getWhereString(StockIndexDTO dto, StringBuilder sb) {
        if (StrUtil.isNotBlank((dto.getCode()))) {
            sb.append(" AND code = #{dto.code}");
        }
        if (StrUtil.isNotBlank(dto.getStockName())) {
            sb.append(" AND stock_name LIKE CONCAT ( #{dto.stockName},'%')");
        }
        if (StrUtil.isNotBlank(dto.getMarket())) {
            sb.append(" AND market= #{dto.market}");
        }
        if (StrUtil.isNotBlank(dto.getRefDate())) {
            sb.append(" AND ref_date= #{dto.refDate}");
        }
    }
}
