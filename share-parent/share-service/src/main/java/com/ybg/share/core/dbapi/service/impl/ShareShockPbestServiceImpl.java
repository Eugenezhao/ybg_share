package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockPbest;
import com.ybg.share.core.dbapi.service.ShareStockPbestService;
import com.ybg.share.core.dbapi.dao.ShareStockPbestMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 最佳买卖算法记录表 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-29
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockPbestService.class)
@Service
public class ShareShockPbestServiceImpl extends ServiceImpl<ShareStockPbestMapper, ShareStockPbest> implements ShareStockPbestService {

    @Override
    public int saveReplace(ShareStockPbest entity) {
        return baseMapper.saveReplace(entity);
    }
}
