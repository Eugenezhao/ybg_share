package com.ybg.share.core.shareapi.impl;

import cn.hutool.core.text.csv.CsvData;
import com.ybg.share.core.shareapi.AbstractStockFinancialApi;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.share.core.dbapi.entity.ShareFinancialYear;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareFinancialYearService;
import com.ybg.share.utils.ThreadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 主要财务报表（季度）
 */
@Service
public class FinancialYearApi extends AbstractStockFinancialApi {
    @Autowired
    ShareFinancialYearService shareFinancialYearService;

    @Override
    public ShareStock getStock() {
        return getThreadLocal().get();
    }

    @Override
    public String getUrl() {
        return "http://quotes.money.163.com/service/zycwzb_" + getStock().getCode() + ".html?type=year";
    }

    @Override
    public void toSaveDB(CsvData csvData) {
        ShareStock stock = getStock();

        int headerSize = csvData.getRow(0).getRawList().size();
        //实际拥有的报告数
        int dataSize = headerSize - 1;

        for (int rowi = 1; rowi < dataSize; rowi++) {
            int count = 0;
            ShareFinancialYear bean = new ShareFinancialYear();
            bean.setReportDate(csvData.getRow(count++).get(rowi));
            bean.setStockId(stock.getId());
            bean.setCode(stock.getCode());
            bean.setBasicEps(toNum(csvData.getRow(count++).get(rowi)));
            bean.setEps(toNum(csvData.getRow(count++).get(rowi)));
            bean.setCashFlow((csvData.getRow(count++).get(rowi)));
            bean.setRevenue(toNum(csvData.getRow(count++).get(rowi)));
            bean.setProfitability(toNum(csvData.getRow(count++).get(rowi)));
            bean.setOperatingProfit(toNum(csvData.getRow(count++).get(rowi)));
            bean.setInvestmentReturn(toNum(csvData.getRow(count++).get(rowi)));
            bean.setNonOperatingIncome(toNum(csvData.getRow(count++).get(rowi)));
            bean.setTotalProfit(toNum(csvData.getRow(count++).get(rowi)));
            bean.setNetProfit(toNum(csvData.getRow(count++).get(rowi)));
            bean.setDeductProfit(toNum(csvData.getRow(count++).get(rowi)));
            bean.setNetCashFlow(toNum(csvData.getRow(count++).get(rowi)));
            bean.setNetIncreaseCash(toNum(csvData.getRow(count++).get(rowi)));
            bean.setTotalAssets(toNum(csvData.getRow(count++).get(rowi)));
            bean.setCurrentAssets(toNum(csvData.getRow(count++).get(rowi)));
            bean.setTotalLiabilities(toNum(csvData.getRow(count++).get(rowi)));
            bean.setCurrentLiabilities(toNum(csvData.getRow(count++).get(rowi)));
            bean.setStockholderEquity(toNum(csvData.getRow(count++).get(rowi)));
            bean.setWeightedReturnEquity(toNum(csvData.getRow(count++).get(rowi)));
            bean.setMarket(stock.getMarket());
//            ThreadUtil.execute(() -> {
//                shareFinancialYearService.saveIgnore(bean);
//
//            });
            ThreadUtil.execute(new AbstractJob(bean) {
                @Override
                public void execute() {
                    ShareFinancialYear bean= (ShareFinancialYear) getParams();
                    shareFinancialYearService.saveIgnore(bean);
                }
            });

        }


    }

    private BigDecimal toNum(String value) {
        try {
            return new BigDecimal(value);
        } catch (Exception e) {

        }
        return BigDecimal.ZERO;

    }
}
