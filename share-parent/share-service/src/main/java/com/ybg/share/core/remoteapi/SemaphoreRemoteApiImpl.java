package com.ybg.share.core.remoteapi;

import com.ybg.share.core.remoteapi.vo.SemaphoreVO;
import com.ybg.share.utils.ThreadUtil;
import org.springframework.stereotype.Service;


@Service
public class SemaphoreRemoteApiImpl implements SemaphoreRemoteApi {


    @Override
    public SemaphoreVO getThreadSemaphore() {
        SemaphoreVO vo = new SemaphoreVO();
        vo.setAvailablePermits(ThreadUtil.THREAD_SEMAPHORE.availablePermits());
        vo.setDrainPermits(ThreadUtil.THREAD_SEMAPHORE.drainPermits());
        vo.setQueueLength(ThreadUtil.THREAD_SEMAPHORE.getQueueLength());
        vo.setHasQueuedThreads(ThreadUtil.THREAD_SEMAPHORE.hasQueuedThreads());
        vo.setIpAddr("");
        return vo;
    }
}
