package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;

/**
 * <p>
 * 财务报表-季度 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
public interface ShareFinancialQuarterService extends IService<ShareFinancialQuarter> {

    boolean saveIgnore(ShareFinancialQuarter bean);

    public Page<FinancialVO> getFinancialOfQuarterByPage(FinancialDTO dto);

}
