package com.ybg.share.core.pbest.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
@Data
public class PbestVO implements Serializable {
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer refYear;
    /**
     * 买入价格
     */
    @ApiModelProperty(value = "买入价格")
    private BigDecimal buyPrice;
    /**
     * 卖出价格
     */
    @ApiModelProperty(value = "卖出价格")
    private BigDecimal sellPrice;
    /**
     * 算法名称
     */
    @ApiModelProperty(value = "算法名称")
    private String pbestName;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 最大次数
     */
    @ApiModelProperty(value = "最大次数")
    private Integer times;
    /**
     * 盈利
     */
    @ApiModelProperty(value = "盈利")
    private BigDecimal profit;
    /**
     * 利润率
     */
    @ApiModelProperty(value = "利润率")
    private BigDecimal profitRate;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 市场
     */
    @ApiModelProperty(value = "市场")
    private String market;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;
}
