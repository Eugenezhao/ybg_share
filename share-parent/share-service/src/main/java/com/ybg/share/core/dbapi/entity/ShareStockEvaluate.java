package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 千股千评
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
@ApiModel(value = "千股千评")
public class ShareStockEvaluate extends Model<ShareStockEvaluate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;
    /**
     * 最后一次更新时间
     */
    @ApiModelProperty(value = "最后一次更新时间")
    private LocalDateTime lastUpdateTime;
    /**
     * 涨跌幅
     */
    @ApiModelProperty(value = "涨跌幅")
    private BigDecimal changePercent;
    /**
     * 关注指数
     */
    @ApiModelProperty(value = "关注指数")
    private BigDecimal focus;
    /**
     * 机构参与度
     */
    @ApiModelProperty(value = "机构参与度")
    private BigDecimal jgcyd;
    /**
     * 机构参与度描述
     */
    @ApiModelProperty(value = "机构参与度描述")
    private String jgcydType;
    /**
     * 最新价
     */
    @ApiModelProperty(value = "最新价")
    private BigDecimal nowPrice;
    /**
     * 排名上升
     */
    @ApiModelProperty(value = "排名上升")
    private Integer rankingUp;
    /**
     * 综合得分
     */
    @ApiModelProperty(value = "综合得分")
    private BigDecimal totalScore;
    /**
     * 换手率
     */
    @ApiModelProperty(value = "换手率")
    private BigDecimal turnoverRate;
    /**
     * 主力成本
     */
    @ApiModelProperty(value = "主力成本")
    private BigDecimal zlcb;
    /**
     * 主力成本20日
     */
    @ApiModelProperty(value = "主力成本20日")
    private BigDecimal zlcb20r;
    /**
     * 主力成本六十日
     */
    @ApiModelProperty(value = "主力成本六十日")
    private BigDecimal zlcb60r;
    /**
     * 主力净流入
     */
    @ApiModelProperty(value = "主力净流入")
    private BigDecimal zljlr;
    /**
     * 当前排名
     */
    @ApiModelProperty(value = "当前排名")
    private Integer ranking;
    /**
     * 市盈率
     */
    @ApiModelProperty(value = "市盈率")
    private BigDecimal peRation;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     */

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 获取股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     */

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    /**
     * 获取最后一次更新时间
     */
    public LocalDateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * 设置最后一次更新时间
     */

    public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * 获取涨跌幅
     */
    public BigDecimal getChangePercent() {
        return changePercent;
    }

    /**
     * 设置涨跌幅
     */

    public void setChangePercent(BigDecimal changePercent) {
        this.changePercent = changePercent;
    }

    /**
     * 获取关注指数
     */
    public BigDecimal getFocus() {
        return focus;
    }

    /**
     * 设置关注指数
     */

    public void setFocus(BigDecimal focus) {
        this.focus = focus;
    }

    /**
     * 获取机构参与度
     */
    public BigDecimal getJgcyd() {
        return jgcyd;
    }

    /**
     * 设置机构参与度
     */

    public void setJgcyd(BigDecimal jgcyd) {
        this.jgcyd = jgcyd;
    }

    /**
     * 获取机构参与度描述
     */
    public String getJgcydType() {
        return jgcydType;
    }

    /**
     * 设置机构参与度描述
     */

    public void setJgcydType(String jgcydType) {
        this.jgcydType = jgcydType;
    }

    /**
     * 获取最新价
     */
    public BigDecimal getNowPrice() {
        return nowPrice;
    }

    /**
     * 设置最新价
     */

    public void setNowPrice(BigDecimal nowPrice) {
        this.nowPrice = nowPrice;
    }

    /**
     * 获取排名上升
     */
    public Integer getRankingUp() {
        return rankingUp;
    }

    /**
     * 设置排名上升
     */

    public void setRankingUp(Integer rankingUp) {
        this.rankingUp = rankingUp;
    }

    /**
     * 获取综合得分
     */
    public BigDecimal getTotalScore() {
        return totalScore;
    }

    /**
     * 设置综合得分
     */

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    /**
     * 获取换手率
     */
    public BigDecimal getTurnoverRate() {
        return turnoverRate;
    }

    /**
     * 设置换手率
     */

    public void setTurnoverRate(BigDecimal turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    /**
     * 获取主力成本
     */
    public BigDecimal getZlcb() {
        return zlcb;
    }

    /**
     * 设置主力成本
     */

    public void setZlcb(BigDecimal zlcb) {
        this.zlcb = zlcb;
    }

    /**
     * 获取主力成本20日
     */
    public BigDecimal getZlcb20r() {
        return zlcb20r;
    }

    /**
     * 设置主力成本20日
     */

    public void setZlcb20r(BigDecimal zlcb20r) {
        this.zlcb20r = zlcb20r;
    }

    /**
     * 获取主力成本六十日
     */
    public BigDecimal getZlcb60r() {
        return zlcb60r;
    }

    /**
     * 设置主力成本六十日
     */

    public void setZlcb60r(BigDecimal zlcb60r) {
        this.zlcb60r = zlcb60r;
    }

    /**
     * 获取主力净流入
     */
    public BigDecimal getZljlr() {
        return zljlr;
    }

    /**
     * 设置主力净流入
     */

    public void setZljlr(BigDecimal zljlr) {
        this.zljlr = zljlr;
    }

    /**
     * 获取当前排名
     */
    public Integer getRanking() {
        return ranking;
    }

    /**
     * 设置当前排名
     */

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    /**
     * 获取市盈率
     */
    public BigDecimal getPeRation() {
        return peRation;
    }

    /**
     * 设置市盈率
     */

    public void setPeRation(BigDecimal peRation) {
        this.peRation = peRation;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String STOCK_CODE = "stock_code";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String STOCK_NAME = "stock_name";

    /**
     * 最后一次更新时间列的数据库字段名称
     */
    public static final String LAST_UPDATE_TIME = "last_update_time";

    /**
     * 涨跌幅列的数据库字段名称
     */
    public static final String CHANGE_PERCENT = "change_percent";

    /**
     * 关注指数列的数据库字段名称
     */
    public static final String FOCUS = "focus";

    /**
     * 机构参与度列的数据库字段名称
     */
    public static final String JGCYD = "jgcyd";

    /**
     * 机构参与度描述列的数据库字段名称
     */
    public static final String JGCYD_TYPE = "jgcyd_type";

    /**
     * 最新价列的数据库字段名称
     */
    public static final String NOW_PRICE = "now_price";

    /**
     * 排名上升列的数据库字段名称
     */
    public static final String RANKING_UP = "ranking_up";

    /**
     * 综合得分列的数据库字段名称
     */
    public static final String TOTAL_SCORE = "total_score";

    /**
     * 换手率列的数据库字段名称
     */
    public static final String TURNOVER_RATE = "turnover_rate";

    /**
     * 主力成本列的数据库字段名称
     */
    public static final String ZLCB = "zlcb";

    /**
     * 主力成本20日列的数据库字段名称
     */
    public static final String ZLCB20R = "zlcb20r";

    /**
     * 主力成本六十日列的数据库字段名称
     */
    public static final String ZLCB60R = "zlcb60r";

    /**
     * 主力净流入列的数据库字段名称
     */
    public static final String ZLJLR = "zljlr";

    /**
     * 当前排名列的数据库字段名称
     */
    public static final String RANKING = "ranking";

    /**
     * 市盈率列的数据库字段名称
     */
    public static final String PE_RATION = "pe_ration";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockEvaluate{" +
                "id=" + id +
                ", stockId=" + stockId +
                ", stockCode=" + stockCode +
                ", stockName=" + stockName +
                ", lastUpdateTime=" + lastUpdateTime +
                ", changePercent=" + changePercent +
                ", focus=" + focus +
                ", jgcyd=" + jgcyd +
                ", jgcydType=" + jgcydType +
                ", nowPrice=" + nowPrice +
                ", rankingUp=" + rankingUp +
                ", totalScore=" + totalScore +
                ", turnoverRate=" + turnoverRate +
                ", zlcb=" + zlcb +
                ", zlcb20r=" + zlcb20r +
                ", zlcb60r=" + zlcb60r +
                ", zljlr=" + zljlr +
                ", ranking=" + ranking +
                ", peRation=" + peRation +
                "}";
    }
}
