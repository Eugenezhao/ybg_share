package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 *
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
@ApiModel(value = "")
public class ShareStockEmailData extends Model<ShareStockEmailData> {

    private static final long serialVersionUID = 1L;

    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer refYear;
    /**
     * 买入价格
     */
    @ApiModelProperty(value = "买入价格")
    private BigDecimal buyPrice;
    /**
     * 卖出价格
     */
    @ApiModelProperty(value = "卖出价格")
    private BigDecimal sellPrice;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 最大次数
     */
    @ApiModelProperty(value = "最大次数")
    private Integer times;
    /**
     * 盈利
     */
    @ApiModelProperty(value = "盈利")
    private BigDecimal profit;
    /**
     * 利润率
     */
    @ApiModelProperty(value = "利润率")
    private BigDecimal profitRate;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 市场
     */
    @ApiModelProperty(value = "市场")
    private String market;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;
    /**
     * 当前价，取自前一个日K收盘价
     */
    @ApiModelProperty(value = "当前价，取自前一个日K收盘价")
    private BigDecimal price;
    /**
     * 差异比,数字越大则利润率越小
     */
    @ApiModelProperty(value = "差异比,数字越大则利润率越小")
    private BigDecimal oddsRatio;
    /**
     * 关注指数
     */
    @ApiModelProperty(value = "关注指数")
    private BigDecimal focus;
    /**
     * 机构参与度
     */
    @ApiModelProperty(value = "机构参与度")
    private BigDecimal jgcyd;
    /**
     * 机构参与度描述
     */
    @ApiModelProperty(value = "机构参与度描述")
    private String jgcydType;
    /**
     * 总和得分
     */
    @ApiModelProperty(value = "总和得分")
    private BigDecimal totalScore;
    /**
     * 当前排名
     */
    @ApiModelProperty(value = "当前排名")
    private Integer ranking;
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 市盈率
     */
    @ApiModelProperty(value = "市盈率")
    private BigDecimal peRation;


    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取年份
     */
    public Integer getRefYear() {
        return refYear;
    }

    /**
     * 设置年份
     */

    public void setRefYear(Integer refYear) {
        this.refYear = refYear;
    }

    /**
     * 获取买入价格
     */
    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    /**
     * 设置买入价格
     */

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    /**
     * 获取卖出价格
     */
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置卖出价格
     */

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取创建时间
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     */

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最大次数
     */
    public Integer getTimes() {
        return times;
    }

    /**
     * 设置最大次数
     */

    public void setTimes(Integer times) {
        this.times = times;
    }

    /**
     * 获取盈利
     */
    public BigDecimal getProfit() {
        return profit;
    }

    /**
     * 设置盈利
     */

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    /**
     * 获取利润率
     */
    public BigDecimal getProfitRate() {
        return profitRate;
    }

    /**
     * 设置利润率
     */

    public void setProfitRate(BigDecimal profitRate) {
        this.profitRate = profitRate;
    }

    /**
     * 获取股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     */

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 获取市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     */

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    /**
     * 获取当前价，取自前一个日K收盘价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置当前价，取自前一个日K收盘价
     */

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 获取差异比,数字越大则利润率越小
     */
    public BigDecimal getOddsRatio() {
        return oddsRatio;
    }

    /**
     * 设置差异比,数字越大则利润率越小
     */

    public void setOddsRatio(BigDecimal oddsRatio) {
        this.oddsRatio = oddsRatio;
    }

    /**
     * 获取关注指数
     */
    public BigDecimal getFocus() {
        return focus;
    }

    /**
     * 设置关注指数
     */

    public void setFocus(BigDecimal focus) {
        this.focus = focus;
    }

    /**
     * 获取机构参与度
     */
    public BigDecimal getJgcyd() {
        return jgcyd;
    }

    /**
     * 设置机构参与度
     */

    public void setJgcyd(BigDecimal jgcyd) {
        this.jgcyd = jgcyd;
    }

    /**
     * 获取机构参与度描述
     */
    public String getJgcydType() {
        return jgcydType;
    }

    /**
     * 设置机构参与度描述
     */

    public void setJgcydType(String jgcydType) {
        this.jgcydType = jgcydType;
    }

    /**
     * 获取总和得分
     */
    public BigDecimal getTotalScore() {
        return totalScore;
    }

    /**
     * 设置总和得分
     */

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    /**
     * 获取当前排名
     */
    public Integer getRanking() {
        return ranking;
    }

    /**
     * 设置当前排名
     */

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取市盈率
     */
    public BigDecimal getPeRation() {
        return peRation;
    }

    /**
     * 设置市盈率
     */

    public void setPeRation(BigDecimal peRation) {
        this.peRation = peRation;
    }

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 年份列的数据库字段名称
     */
    public static final String REF_YEAR = "ref_year";

    /**
     * 买入价格列的数据库字段名称
     */
    public static final String BUY_PRICE = "buy_price";

    /**
     * 卖出价格列的数据库字段名称
     */
    public static final String SELL_PRICE = "sell_price";

    /**
     * 创建时间列的数据库字段名称
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 最大次数列的数据库字段名称
     */
    public static final String TIMES = "times";

    /**
     * 盈利列的数据库字段名称
     */
    public static final String PROFIT = "profit";

    /**
     * 利润率列的数据库字段名称
     */
    public static final String PROFIT_RATE = "profit_rate";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String STOCK_CODE = "stock_code";

    /**
     * 市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String STOCK_NAME = "stock_name";

    /**
     * 当前价，取自前一个日K收盘价列的数据库字段名称
     */
    public static final String PRICE = "price";

    /**
     * 差异比,数字越大则利润率越小列的数据库字段名称
     */
    public static final String ODDS_RATIO = "odds_ratio";

    /**
     * 关注指数列的数据库字段名称
     */
    public static final String FOCUS = "focus";

    /**
     * 机构参与度列的数据库字段名称
     */
    public static final String JGCYD = "jgcyd";

    /**
     * 机构参与度描述列的数据库字段名称
     */
    public static final String JGCYD_TYPE = "jgcyd_type";

    /**
     * 总和得分列的数据库字段名称
     */
    public static final String TOTAL_SCORE = "total_score";

    /**
     * 当前排名列的数据库字段名称
     */
    public static final String RANKING = "ranking";

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 市盈率列的数据库字段名称
     */
    public static final String PE_RATION = "pe_ration";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockEmailData{" +
                "stockId=" + stockId +
                ", refYear=" + refYear +
                ", buyPrice=" + buyPrice +
                ", sellPrice=" + sellPrice +
                ", createTime=" + createTime +
                ", times=" + times +
                ", profit=" + profit +
                ", profitRate=" + profitRate +
                ", stockCode=" + stockCode +
                ", market=" + market +
                ", stockName=" + stockName +
                ", price=" + price +
                ", oddsRatio=" + oddsRatio +
                ", focus=" + focus +
                ", jgcyd=" + jgcyd +
                ", jgcydType=" + jgcydType +
                ", totalScore=" + totalScore +
                ", ranking=" + ranking +
                ", id=" + id +
                ", peRation=" + peRation +
                "}";
    }
}
