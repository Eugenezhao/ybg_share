package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.dbapi.entity.ShareStockPbest;

/**
 * <p>
 * 最佳买卖算法记录表 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-29
 */
public interface ShareStockPbestService extends IService<ShareStockPbest> {

    int saveReplace(ShareStockPbest entity);
}
