package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import java.io.Serializable;


/**
 * <p>
 * 会员接收邮箱的列表
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
@ApiModel(value = "会员接收邮箱的列表")
public class ShareReceiveEmail extends Model<ShareReceiveEmail> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     * 服务起始时间
     */
    @ApiModelProperty(value = "服务起始时间")
    private LocalDateTime startTime;
    /**
     * 服务结束时间
     */
    @ApiModelProperty(value = "服务结束时间")
    private LocalDateTime endTime;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     */

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     */

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取服务起始时间
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * 设置服务起始时间
     */

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取服务结束时间
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * 设置服务结束时间
     */

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 邮箱列的数据库字段名称
     */
    public static final String EMAIL = "email";

    /**
     * 备注列的数据库字段名称
     */
    public static final String REMARK = "remark";

    /**
     * 服务起始时间列的数据库字段名称
     */
    public static final String START_TIME = "start_time";

    /**
     * 服务结束时间列的数据库字段名称
     */
    public static final String END_TIME = "end_time";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareReceiveEmail{" +
                "id=" + id +
                ", email=" + email +
                ", remark=" + remark +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                "}";
    }
}
