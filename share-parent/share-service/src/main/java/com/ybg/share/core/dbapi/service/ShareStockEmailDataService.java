package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockEmailData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
public interface ShareStockEmailDataService extends IService<ShareStockEmailData> {

}
