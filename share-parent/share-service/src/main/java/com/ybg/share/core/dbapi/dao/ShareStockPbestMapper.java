package com.ybg.share.core.dbapi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.share.core.dbapi.entity.ShareStockPbest;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 最佳买卖算法记录表 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-29
 */
public interface ShareStockPbestMapper extends BaseMapper<ShareStockPbest> {

    @Insert("INSERT INTO share_stock_pbest(stock_id,ref_year,buy_price,sell_price,pbest_name,update_time,create_time,times,profit,profit_rate,stock_code,market,stock_name) " +
            " VALUES(#{stockId},#{refYear},#{buyPrice},#{sellPrice},#{pbestName},#{updateTime},#{createTime},#{times},#{profit},#{profitRate},#{stockCode},#{market},#{stockName})"
    +" ON DUPLICATE KEY UPDATE stock_id=VALUES( stock_id),ref_year=VALUES( ref_year),buy_price=VALUES( buy_price),sell_price=VALUES( sell_price),pbest_name=VALUES( pbest_name),update_time=VALUES( update_time),create_time=VALUES( create_time),times=VALUES( times),profit=VALUES( profit),profit_rate=VALUES( profit_rate),stock_code=VALUES( stock_code),market=VALUES( market),stock_name=VALUES( stock_name)")
    int saveReplace(ShareStockPbest entity);
}
