package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockGravity;
import com.ybg.share.core.dbapi.dao.ShareStockGravityMapper;
import com.ybg.share.core.dbapi.service.ShareStockGravityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 地心引力检测表 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2020-05-27
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockGravityService.class)
@Service
public class ShareStockGravityServiceImpl extends ServiceImpl<ShareStockGravityMapper, ShareStockGravity> implements ShareStockGravityService {

}
