package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockEmailDataHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
public interface ShareStockEmailDataHistoryMapper extends BaseMapper<ShareStockEmailDataHistory> {

}
