package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockEvaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 千股千评 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2020-01-04
 */
public interface ShareStockEvaluateMapper extends BaseMapper<ShareStockEvaluate> {


    @Insert("INSERT INTO share_stock_evaluate(stock_code,stock_name,last_update_time,change_percent,focus,jgcyd,jgcyd_type,now_price,ranking_up,total_score,turnover_rate,zlcb,zlcb20r,zlcb60r,zljlr,ranking,pe_ration)"
    +" VALUES(#{stockCode},#{stockName},#{lastUpdateTime},#{changePercent},#{focus},#{jgcyd},#{jgcydType},#{nowPrice},#{rankingUp},#{totalScore},#{turnoverRate},#{zlcb},#{zlcb20r},#{zlcb60r},#{zljlr},#{ranking},#{peRation})" +
            " ON DUPLICATE KEY UPDATE stock_name=VALUES( stock_name),last_update_time=VALUES( last_update_time),change_percent=VALUES( change_percent),focus=VALUES( focus),jgcyd=VALUES( jgcyd),jgcyd_type=VALUES( jgcyd_type),now_price=VALUES( now_price),ranking_up=VALUES( ranking_up),total_score=VALUES( total_score),turnover_rate=VALUES( turnover_rate),zlcb=VALUES( zlcb),zlcb20r=VALUES( zlcb20r),zlcb60r=VALUES( zlcb60r),zljlr=VALUES( zljlr),ranking=VALUES( ranking),pe_ration=VALUES(pe_ration)")
    void saveIgnore(ShareStockEvaluate shareStockEvaluate);
}
