package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockCountYear;
import com.ybg.share.core.dbapi.dao.sql.ShareStockCountYearSQL;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 个股年度统计 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
public interface ShareStockCountYearMapper extends BaseMapper<ShareStockCountYear> {

    @InsertProvider(type =
            ShareStockCountYearSQL.class, method = "saveReplace")
    int saveReplace(@Param("e") ShareStockCountYear entity);
}
