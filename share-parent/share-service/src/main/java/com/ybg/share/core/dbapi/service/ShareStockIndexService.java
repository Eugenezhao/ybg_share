package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;

/**
 * <p>
 * 上证指数和深证指数 服务类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
public interface ShareStockIndexService extends IService<ShareStockIndex> {

    boolean insertIgnore(ShareStockIndex shareStockIndex);

    Page<StockIndexDayKVO> pageStock(StockIndexDTO dto);
}
