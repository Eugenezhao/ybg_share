package com.ybg.share.config;

import com.ybg.framework.core.NetasetSendEmail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class SystemConfig {
    /**
     * 网易邮箱配置
     *
     * @param account
     * @param password
     * @return
     */
    @Bean
    public NetasetSendEmail netasetSendEmail(
            @Value("${email.netaset.account}") String account,
            @Value("${email.netaset.password}") String password) {
        log.info("系统邮箱：" + account);
        return new NetasetSendEmail(account, password);
    }


}