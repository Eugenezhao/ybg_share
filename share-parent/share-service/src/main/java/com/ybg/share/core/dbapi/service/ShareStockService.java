package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 股票信息 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockService extends IService<ShareStock> {
    /**
     * 保存（重复冲突不覆盖）
     *
     * @param shareStock
     * @return
     */
    boolean saveIgnore(ShareStock shareStock);

    boolean saveReplace(ShareStock shareStock);

    /**
     * 根据股票代码获取股票
     *
     * @param stock
     * @return
     */
    ShareStock getByCode(String stock);

    /**
     * 系统初始化分表
     *
     * @param sql
     */
    void initSystem(String sql);

    Page<ShareStockVO> pageStock(StockPageQueryDTO dto);
}
