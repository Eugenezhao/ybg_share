package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockEvaluate;
import com.ybg.share.core.dbapi.dao.ShareStockEvaluateMapper;
import com.ybg.share.core.dbapi.service.ShareStockEvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 千股千评 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-04
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockEvaluateService.class)
@Service
public class ShareStockEvaluateServiceImpl extends ServiceImpl<ShareStockEvaluateMapper, ShareStockEvaluate> implements ShareStockEvaluateService {

    @Override
    public void saveReplace(ShareStockEvaluate shareStockEvaluate) {
        baseMapper.saveIgnore(shareStockEvaluate);
    }
}
