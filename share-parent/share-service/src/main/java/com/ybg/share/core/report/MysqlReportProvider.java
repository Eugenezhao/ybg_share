package com.ybg.share.core.report;

import com.bstek.ureport.provider.report.ReportFile;
import com.bstek.ureport.provider.report.ReportProvider;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 基于mysql 实现
 * @author  yanyu
 */
public class MysqlReportProvider implements ReportProvider, ApplicationContextAware {

    transient private JdbcTemplate jdbcTemplate;
    private String prefix = "mysql:";

    @Override
    public InputStream loadReport(String file) {

        if (file.startsWith(prefix)) {
            file = file.substring(prefix.length(), file.length());
        }
        List<String> content = jdbcTemplate.query("SELECT content FROM t_tablefiles WHERE name=? LIMIT 1", new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("content");
            }
        }, file);
        return content.size() == 1 ? new ByteArrayInputStream(content.get(0).getBytes()) : null;
    }

    @Override
    public void deleteReport(final String file) {

        String filealis = file;
        if (file.startsWith(prefix)) {
            filealis = file.substring(prefix.length(), file.length());
        }
        jdbcTemplate.update("DELETE FROM t_tablefiles WHERE name=?", filealis);
    }

    @Override
    public List<ReportFile> getReportFiles() {
        return jdbcTemplate.query("SELECT name,update_time FROM t_tablefiles ", new RowMapper<ReportFile>() {

            @Override
            public ReportFile mapRow(ResultSet rs, int rowNum) throws SQLException {

                return new ReportFile(rs.getString("name"), rs.getDate("update_time"));
            }
        });
    }

    @Override
    public void saveReport(String file, String content) {
        String filealis = file;
        if (file.startsWith(prefix)) {
            filealis = file.substring(prefix.length(), file.length());
        }
        System.out.println(filealis+","+file);
        List<String> names = jdbcTemplate.query("SELECT name FROM t_tablefiles WHERE name=? LIMIT 1", new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("name");
            }
        }, filealis);
        if (names.size() == 1) {
            jdbcTemplate.update("update t_tablefiles set update_time =? ,content=? where name=?",
                    new Date(new java.util.Date().getTime()), content, filealis);
        } else {
            jdbcTemplate.update("insert into  t_tablefiles(name,update_time,content) values (?,?,?) ",
                    filealis,new Date(new java.util.Date().getTime()),content );
        }
    }

    @Override
    public String getName() {
        return "mysql存储器";
    }

    @Override
    public boolean disabled() {
        return false;
    }

    @Override
    public String getPrefix() {
        return "mysql:";
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }

    public MysqlReportProvider(DataSource dataSource) {
        if (dataSource == null) {
            throw new RuntimeException("MysqlReportProvider dataSource is Null!");
        }

        jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
