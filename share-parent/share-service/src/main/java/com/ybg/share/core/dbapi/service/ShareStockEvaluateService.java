package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockEvaluate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 千股千评 服务类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-04
 */
public interface ShareStockEvaluateService extends IService<ShareStockEvaluate> {


    void saveReplace(ShareStockEvaluate shareStockEvaluate);
}
