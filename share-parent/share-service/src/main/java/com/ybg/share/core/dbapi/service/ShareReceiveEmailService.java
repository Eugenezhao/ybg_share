package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareReceiveEmail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员接收邮箱的列表 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
public interface ShareReceiveEmailService extends IService<ShareReceiveEmail> {

}
