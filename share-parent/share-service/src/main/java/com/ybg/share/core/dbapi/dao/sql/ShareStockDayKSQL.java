package com.ybg.share.core.dbapi.dao.sql;


import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.sql.PagerUtils;
import com.alibaba.druid.util.JdbcConstants;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import org.apache.ibatis.annotations.Param;

/**
 * {@link ShareStockDayKMapper}
 */
public class ShareStockDayKSQL {

    public String listStockDayK(@Param("dto") StockDayKDTO dto){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT id,ref_date,code,name,market,close_price,max_price,min_price,open_price,");
        sb.append("before_close,change_amount,change_range,turnover_rate,trade_num,");
        sb.append("trade_money,total_value,circulation_value,turnover_num,stock_id ");
        sb.append("FROM share_stock_day_k ");
        sb.append(" WHERE 1=1 ");
        getWhereString(dto,sb);
        return PagerUtils.limit(sb.toString(), JdbcConstants.MYSQL,dto.getOffset().intValue(),dto.getSize().intValue());
    }

    public String countStockDayK(@Param("dto") StockDayKDTO dto){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(id) FROM share_stock_day_k WHERE 1=1 ");
        getWhereString(dto,sb);
        return PagerUtils.count(sb.toString(), JdbcConstants.MYSQL);
    }

    private void getWhereString(StockDayKDTO dto, StringBuilder sb){
        if (StrUtil.isNotBlank(dto.getStockIds())){
            sb.append(" AND stock_id in (#{dto.stockIds}").append(")");
        }else if (null != dto.getStockId()){
            sb.append(" AND stock_id = #{dto.stockId}");
        }
        if (StrUtil.isNotBlank(dto.getCode())){
            sb.append(" AND code = #{dto.code}");
        }
        if (StrUtil.isNotBlank(dto.getStarTime())){
            sb.append(" AND ref_date = #{dto.startTime}");
        }
    }

    public String initTable(@Param("mod") int mod) {
        return "CREATE TABLE IF NOT EXISTS `share_stock_day_k" + mod + "` ( " +
                "  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键', " +
                "  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16', " +
                "  `code` varchar(20) DEFAULT NULL COMMENT '股票代码', " +
                "  `name` varchar(60) DEFAULT NULL COMMENT '股票名称', " +
                "  `market` varchar(20) DEFAULT NULL COMMENT '所属市场', " +
                "  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价', " +
                "  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价', " +
                "  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价', " +
                "  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价', " +
                "  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘', " +
                "  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额', " +
                "  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅', " +
                "  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率', " +
                "  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量', " +
                "  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额', " +
                "  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值', " +
                "  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值', " +
                "  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量', " +
                "  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID', " +
                "  PRIMARY KEY (`id`), " +
                "  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`), " +
                "  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE " +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';";
    }
}
