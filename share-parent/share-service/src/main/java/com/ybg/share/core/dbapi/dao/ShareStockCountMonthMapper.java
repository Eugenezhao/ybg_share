package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockCountMonth;
import com.ybg.share.core.dbapi.dao.sql.ShareStockCountMonthSQL;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

/**
 * <p>
 * 个股月度统计 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
public interface ShareStockCountMonthMapper extends BaseMapper<ShareStockCountMonth> {


    @UpdateProvider(type = ShareStockCountMonthSQL.class, method = "initTable")
    int initTable(@Param("i") int i);

   @InsertProvider(type = ShareStockCountMonthSQL.class,method = "saveReplace")
    int saveReplace(@Param("e") ShareStockCountMonth entity);
}
