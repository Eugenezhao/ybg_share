package com.ybg.share.core.pbest.dto;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
public class PbestDTO implements Serializable {
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;
    private List<ShareStockDayK> data;

    public PbestDTO(BigDecimal buyPrice, BigDecimal sellPrice, List<ShareStockDayK> data) {
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.data = data;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 计算利润，
     *
     * @return
     */
    public BigDecimal getPbestOutPut() {
        boolean isCanBuy = true;//是否可以买
        boolean isCanSell = false;//是否可以卖
        BigDecimal profit = BigDecimal.ZERO;//利润
        Integer times = 0;//获利次数
        //利润=（差价）*次数
        if (data == null || buyPrice == null || sellPrice == null) {
            throw new RuntimeException("输入值不合法");
        }
        for (ShareStockDayK dayK : data) {
            if (isCanBuy && dayK.getMinPrice().compareTo(buyPrice) < 1) {
                isCanBuy = false;
                isCanSell = true;
            }
            if (isCanSell && dayK.getMaxPrice().compareTo(sellPrice) > -1) {
                times++;
                isCanBuy = true;
                isCanSell = false;
            }
        }

        profit = sellPrice.subtract(buyPrice).multiply(new BigDecimal(times));
        //  log.info("("+sellPrice+"-"+buyPrice+")*"+times+"="+profit);
        return profit;
    }

}
