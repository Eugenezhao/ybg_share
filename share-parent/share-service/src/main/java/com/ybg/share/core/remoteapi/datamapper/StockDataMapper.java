package com.ybg.share.core.remoteapi.datamapper;

import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StockDataMapper {
     StockDataMapper INSTANCES = Mappers.getMapper(StockDataMapper.class);
    ShareStockVO toVO(ShareStock bean);
    List<ShareStockVO> toVOList(List<ShareStock> list);
}
