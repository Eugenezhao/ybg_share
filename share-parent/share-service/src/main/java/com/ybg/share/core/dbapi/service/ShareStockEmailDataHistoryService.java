package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockEmailDataHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
public interface ShareStockEmailDataHistoryService extends IService<ShareStockEmailDataHistory> {

}
