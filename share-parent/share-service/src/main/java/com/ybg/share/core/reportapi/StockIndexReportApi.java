package com.ybg.share.core.reportapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareStockGravity;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 上证指数 报告接口
 * @author  deament
 */
@Component("stockIndexReportApi")
public class StockIndexReportApi {
    @Autowired
    ShareStockIndexService shareStockIndexService;

    /**
     * 地心引力线分析，上证指数不一样，百分之十的点最敏感
     * @param dataSource
     * @param table
     * @param params
     * @return
     */
    public ShareStockGravity getShanghaiIndex(String dataSource, String table, Map<String,String> params){
        QueryWrapper queryWrapper= new QueryWrapper<ShareStockIndex>();
        queryWrapper.eq(ShareStockIndex.CODE, "000001");
        queryWrapper.orderByDesc(ShareStockIndex.REF_DATE);
        queryWrapper.last(" LIMIT 72");
        List<ShareStockIndex> dayKList = shareStockIndexService.list(queryWrapper);
        //求三十日均线和72日均线的和除以2
        Double avg72 = dayKList.stream().collect(Collectors.averagingDouble(x -> x.getClosePrice().doubleValue()));
        Double avg30 = dayKList.subList(0, 30).stream().collect(Collectors.averagingDouble(x -> x.getClosePrice().doubleValue()));
        // 倒序列表
        BigDecimal closePrice = dayKList.get(0).getClosePrice();
        Double gravityValue = (avg72 + avg30) / 2;
        BigDecimal result = new BigDecimal(gravityValue.toString()).setScale(3, RoundingMode.UP);
        ShareStockGravity shareStockGravity= new ShareStockGravity();
        shareStockGravity.setLinePrice120(result.multiply(new BigDecimal("1.2")).setScale(3, RoundingMode.UP));
        shareStockGravity.setLinePrice110(result.multiply(new BigDecimal("1.1")).setScale(3, RoundingMode.UP));
        shareStockGravity.setLinePrice90(result.multiply(new BigDecimal("0.9")).setScale(3, RoundingMode.UP));
        shareStockGravity.setLinePrice80(result.multiply(new BigDecimal("0.8")).setScale(3, RoundingMode.UP));
        shareStockGravity.setLinePrice(result);
        shareStockGravity.setDescription("");
        shareStockGravity.setNowPrice(closePrice);
        shareStockGravity.setStockCode("000001");
        shareStockGravity.setStockName("上证指数");
        if(closePrice.compareTo(BigDecimal.ZERO)==0){
            shareStockGravity.setRate(BigDecimal.ZERO);
        }else{
            shareStockGravity.setRate(closePrice.divide(result,3).setScale(3, RoundingMode.UP));
        }
       getDescription(shareStockGravity);
        return shareStockGravity;
    }

    private void getDescription(ShareStockGravity shareStockGravity){

        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice120())>0){
            shareStockGravity.setDescription("大盘冲高超过引力线超过1.2倍，做空力量极强");
            return;
        }
        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice110())>0){
            shareStockGravity.setDescription("大盘冲高超过引力线超过1.1倍，未来可能有回落");
            return;
        }
        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice110())<0
        && shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice())>=0
        ){
            shareStockGravity.setDescription("大盘震荡上行几个点");
            return;
        }
        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice())<0
                && shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice90())>=0
        ){
            shareStockGravity.setDescription("大盘震荡下行几个点");
            return;
        }
        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice90())<0
                && shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice80())>=0
        ){
            shareStockGravity.setDescription("下行了十几个点，千载难逢");
            return;
        }
        if(shareStockGravity.getNowPrice().compareTo(shareStockGravity.getLinePrice80())<0

        ){
            shareStockGravity.setDescription("下行了二十几个点，千载难逢");
            return;
        }

    }



}
