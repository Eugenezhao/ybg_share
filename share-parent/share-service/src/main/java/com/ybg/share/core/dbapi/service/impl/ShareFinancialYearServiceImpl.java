package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.dbapi.service.ShareFinancialYearService;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.share.core.dbapi.entity.ShareFinancialYear;
import com.ybg.share.core.dbapi.dao.ShareFinancialYearMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 财务报表 年度 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
//@Service(version = "1.0.0", interfaceClass = ShareFinancialYearService.class)
@Service
public class ShareFinancialYearServiceImpl extends ServiceImpl<ShareFinancialYearMapper, ShareFinancialYear> implements ShareFinancialYearService {

    @Override
    public boolean saveIgnore(ShareFinancialYear bean) {
        return baseMapper.saveIgnore(bean);
    }

    @Override
    public Page<FinancialVO> getFinancialOfYearByPage(FinancialDTO dto) {
        //todo
        return null;
    }

    @Override
    public ShareFinancialYear getByYear(Long stockId, int year) {
        QueryWrapper<ShareFinancialYear> wrapper= new QueryWrapper<>();
        wrapper.eq(ShareFinancialYear.STOCK_ID,stockId);
        wrapper.eq(ShareFinancialYear.REPORT_DATE,year+"-12-31");
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public List<ShareFinancialYear> getByYear(int year) {
        QueryWrapper<ShareFinancialYear> wrapper= new QueryWrapper<>();
        wrapper.eq(ShareFinancialYear.REPORT_DATE,year+"-12-31");
        return baseMapper.selectList(wrapper);
    }
}
