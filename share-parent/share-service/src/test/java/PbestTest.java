import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.ShareApplication;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.pbest.dto.PbestDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class PbestTest {
    @Autowired
    ShareStockDayKService shareStockDayKService;

    @Test
    public void test() {
        QueryWrapper<ShareStockDayK> wrapper = new QueryWrapper<>();

        wrapper.eq(ShareStockDayK.STOCK_ID, 1L);
        wrapper.gt(ShareStockDayK.REF_DATE, "2019-01-01");
        wrapper.orderByAsc(ShareStockDayK.REF_DATE);
        BigDecimal maxProfit = BigDecimal.ZERO;
        PbestDTO maxDTO = null;
        System.out.println("查询数据库");
        List<ShareStockDayK> list = shareStockDayKService.list(wrapper);
        System.out.println("查询数据库完毕");
        List<BigDecimal> buylist = buyList(new BigDecimal("13"), new BigDecimal("15"));
        List<BigDecimal> selllist = sellList(new BigDecimal("14"), new BigDecimal("18"));
        System.out.println(buylist.size()+","+selllist.size());
        for (BigDecimal buy : buylist) {
            for (BigDecimal sell : selllist) {
                if (buy.compareTo(sell) == -1) {
                    PbestDTO dto = new PbestDTO(buy, sell, list);
                   BigDecimal profit= dto.getPbestOutPut();
                    if (profit.compareTo(maxProfit) > 0) {
                        maxProfit = profit;
                        maxDTO = dto;
                    }
                }
            }
        }
////        PbestDTO dto = new PbestDTO(new BigDecimal("14"), new BigDecimal("15"), list);
        //System.out.println("次数"+ maxProfit.divide(maxDTO.getSellPrice().subtract(maxDTO.getBuyPrice())) );
        System.out.println("买入价:" + maxDTO.getBuyPrice() + ",卖出价：" + maxDTO.getSellPrice() + "最大利润= " + maxProfit);


    }

    private List<BigDecimal> buyList(BigDecimal minPrice, BigDecimal maxPrice) {
        List<BigDecimal> list = new ArrayList<>();
        BigDecimal minPrice2 = minPrice;
         do{
            list.add(minPrice2);
            minPrice2=  minPrice2.add(new BigDecimal("0.01"));

        } while (minPrice2.compareTo(maxPrice) < 0);
        return list;
    }

    private List<BigDecimal> sellList(BigDecimal minPrice, BigDecimal maxPrice) {
        List<BigDecimal> list = new ArrayList<>();
        BigDecimal minPrice2 = minPrice;
        do {
            list.add(minPrice2);
            minPrice2=  minPrice2.add(new BigDecimal("0.01"));

        } while (minPrice2.compareTo(maxPrice) < 0);
        return list;
    }


}
