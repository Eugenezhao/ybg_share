package com.ybg.framework.constant;

public interface RedisKey extends SystemRedisKey {
    /**
     * 最新的日K列表
     */
    String LAST_DAY_K = "LAST_DAY_K";
    /**
     * 财务年度数据列表+年份
     */
    String SHARE_FINANCIAL_YEAR="SHARE_FINANCIAL_YEAR:";

}
