package com.ybg.framework.dto;


import java.io.Serializable;

/**
 * 基础分页类
 */
public class BasePageDTO implements Serializable {
    private static final long serialVersionUID = -4612875364166624116L;

    private Long size;

    private Long currentPage = 1L;

    public BasePageDTO() {
    }

    public Long getSize() {
        return this.size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getCurrentPage() {
        return this.currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    /***
     * 这个参数时用于手写分页计算出 offset的值
     * @return
     */
    public Long getOffset() {
        return this.currentPage != null && this.currentPage > 0L && this.size != null ? (this.currentPage - 1L) * this.size : 0L;
    }
}